#!/bin/bash

check_args() {
    if [[ $1 -eq 0 ]] ; then
        echo "Missing arguments !"
        echo "Usage : ./skeleton/create.sh relative/path/to/new/project [/absolute/path/to/python/interpreter]"
        exit 1
    fi
}

set_python() {
    if [ -n "$1" ] ; then
        python_path=$1
    else
        python_path=`which python3`
    fi
    python_version=`$python_path --version`
}

print_status() {
    if [ "$1" -eq 0 ] ; then
        echo -e "${green}OK${normal}"
    else
        echo -e "${red}KO${normal}"
        exit 1
    fi
}

create_structure() {
    mkdir -p $1/{app,test}
    touch $1/{app,test}/__init__.py
}

copy_conf() {
     cp $skeleton_path/conf/.gitignore $1
     cp $skeleton_path/conf/pytest.ini $1
     cp $skeleton_path/conf/requirements.txt $1
}

compile_template() {
    cd $skeleton_path/templates
    virtualenv -p $2 venv
    . venv/bin/activate
    pip install Jinja2
    python compile.py $1 "$3" $4
    deactivate
    rm -rf venv
}

install_requirements() {
    cd $1
    virtualenv -p $2 venv
    . venv/bin/activate
    pip install -r requirements.txt
    deactivate
}

run_test() {
    . venv/bin/activate
    pytest test
    deactivate
}

init_git() {
    git init
    git add .
    git commit -m "init $1 project with default skeleton"
}

set -e

check_args $#

bold=$(tput bold)
green='\033[0;32m'
red='\033[0;31m'
normal=$(tput sgr0)
start_time=`date +%s`
project_path=$1
python_path=$2
python_version=""
skeleton_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
author="$( git config --get user.name )"
author_email=$( git config --get user.email )

echo "> Setting python version"
set_python $python_path $python_version
print_status $?

echo "> Creating default project structure"
create_structure $project_path
print_status $?

project_absolute_path=`echo $(cd $project_path; pwd)`

echo "> Copying default project configuration"
copy_conf $project_absolute_path
print_status $?

echo "> Generating default source, test and readme files"
compile_template $project_absolute_path $python_path "$author" $author_email
print_status $?

echo "> Installing project requirements"
install_requirements $project_absolute_path $python_path
print_status $?

echo "> Running generated unit test"
run_test $project_absolute_path
print_status $?

echo "> Initiating local Git repository"
init_git $project_absolute_path
print_status $?

end_time=`date +%s`
runtime=$((end_time-start_time))

echo "> Project generated in ${bold}$runtime seconds${normal} in ${bold}$project_absolute_path${normal} with ${bold}$python_version${normal} !"

echo -e "\n"
echo -e "\t########################"
echo -e "\t#                      #"
echo -e "\t#   ${green}${bold}Happy coding :-)${normal}   #"
echo -e "\t#                      #"
echo -e "\t########################"
echo -e "\n"
