# skeleton

The purpose of this script is to generate a new Python project with a default structure.
It will create the following structure, with a virtualenv and a git repository :
```
[project_name]
├── app
│   ├── [project_name].py
│   └── __init__.py
└── test
│   ├── [project_name]_test.py
│   └── __init__.py
├── pytest.ini
├── README.md
├── requirements.txt
├── .gitignore
├── .git
│   └── ...
└── venv
    └── ...

```
## Dependencies

* git
* pip
* virtualenv
* python3

## Usage

The script takes two arguments :
* the path to your new project (mandatory)
* the path of the Python interpreter you want to use (optional)
  * if not provided, it will use whatever `which python3` returns

```
./skeleton/create.sh relative/path/to/new/project
```
or
```
./skeleton/create.sh relative/path/to/new/project /absolute/path/to/python/interpreter
```
