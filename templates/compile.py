"""
    This module compiles Jinja2 templates and writes the results in destination files.
"""
from jinja2 import Template
import os
import sys

project_path = sys.argv[1]
project_name = os.path.basename(project_path)
author = sys.argv[2]
author_email = sys.argv[3]

def to_camel_case(name, separator=''):
    return separator.join([word.capitalize() for word in name.split('_')])


def compile_template(directory, source, destination):
    with open(source, 'r') as f:
        template = Template(f.read())
    with open('%s/%s' % (directory, destination), 'w') as f:
        content = template.render(
            project_name=project_name,
            author=author,
            author_email=author_email,
            camel_case=to_camel_case
        )
        f.write(content)


compile_template(project_path, 'README.md.j2', 'README.md')
compile_template(project_path + '/app', 'source.py.j2', '%s.py' % project_name)
compile_template(project_path + '/test', 'test.py.j2', '%s_test.py' % project_name)
compile_template(project_path, 'setup.py.j2', 'setup.py')
